//
//  NewsListTableViewCellObject.h
//  ExperementDI
//
//  Created by Alex Zverev on 09.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewsItemPlaneObject.h"
#import <Nimbus/NimbusModels.h>

@interface NewsListTableViewCellObject : NSObject<NICellObject>

@property (strong, nonatomic, readonly) NSString *newsTitle;
@property (strong, nonatomic, readonly) NSString *newsDescription;
@property (strong, nonatomic, readonly) NSString *newsSource;
@property (strong, nonatomic, readonly) NSURL *newsCoverUrl;
@property (nonatomic, strong, readonly) NSString *publicationDate;
@property (strong, nonatomic, readonly) NewsItemPlaneObject *newsItem;
@property (nonatomic) BOOL isSelected;

+ (instancetype)objectWithNewsItem:(NewsItemPlaneObject *)newsItem;

@end
