//
//  RSSNewsParserImplementation.h
//  ExperementDI
//
//  Created by Alex Zverev on 14.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSSNewsParser.h"

@interface RSSNewsParserImplementation : NSObject<RSSNewsParser>

@end
