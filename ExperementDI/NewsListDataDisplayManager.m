//
//  NewsListDataDisplayManager.m
//  ExperementDI
//
//  Created by Alex Zverev on 09.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "NewsListDataDisplayManager.h"
#import "NewsListTableViewCellObject.h"
#import "NewsListTableViewCell.h"
#import "NewsItemPlaneObject.h"
#import <Nimbus/NimbusModels.h>
#import "EXTScope.h"

@interface NewsListDataDisplayManager()<UITableViewDelegate>

@property (strong, nonatomic) NIMutableTableViewModel *tableViewModel;
@property (strong, nonatomic) NITableViewActions *tableViewActions;
@property (strong, nonatomic) NSArray *newsList;
@property (strong, nonatomic) NewsListTableViewCellObject *selectedOjbect;

@end


@implementation NewsListDataDisplayManager

- (void)updateTableViewModelWithNewsList:(NSArray *)newsList {
    self.newsList = newsList;
    [self updateTableViewModel];
    [self.delegate didUpdateTableViewModel];
}

#pragma mark - DataSource and Delegage for TableViewController
//using upon init
//TODO: relocate to protocol
- (id<UITableViewDataSource>)dataSourceForTableView:(UITableView *)tableView {
    if (!self.tableViewModel) {
        [self updateTableViewModel];
    }
    return self.tableViewModel;
}

- (id<UITableViewDelegate>)delegateForTableView:(UITableView *)tableView withBaseDelegate:(id<UITableViewDelegate>)baseTableViewDelegate {
    if (!self.tableViewActions) {
        [self setupTableViewActions];
    }
    return [self.tableViewActions forwardingTo:self];
}

#pragma mark - UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [NICellFactory tableView:tableView heightForRowAtIndexPath:indexPath model:self.tableViewModel];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsListTableViewCellObject *newCellObject = [self.tableViewModel objectAtIndexPath:indexPath];
    
    self.selectedOjbect.isSelected = NO;
    if (newCellObject == self.selectedOjbect) {
        
        self.selectedOjbect = nil;
    } else {
        newCellObject.isSelected = YES;
        self.selectedOjbect =  newCellObject;
    }
    
    [tableView reloadData];
    [tableView beginUpdates];
    [tableView endUpdates];
}

#pragma mark - Private methods

- (void)setupTableViewActions {
    self.tableViewActions = [[NITableViewActions alloc] initWithTarget:self];
    self.tableViewActions.tableViewCellSelectionStyle = UITableViewCellSelectionStyleNone;
    @weakify(self);
    NIActionBlock newsListTapActionBlock = ^BOOL(NewsListTableViewCellObject *object, id target, NSIndexPath *indexPath) {
        @strongify(self);
        [self.delegate didTapCellWithNewsItem:object.newsItem];
        return YES;
    };
    
    [self.tableViewActions attachToClass:[NewsListTableViewCellObject class] tapBlock:newsListTapActionBlock];
}

- (NSArray *)generateCellObjects {
    NSMutableArray *cellObjects = [NSMutableArray array];
    for (int i = 0; i < self.newsList.count; i++) {
        NewsItemPlaneObject *newsItem = [self.newsList objectAtIndex:i];
        
        NewsListTableViewCellObject *newsListCellObject = [NewsListTableViewCellObject objectWithNewsItem:newsItem];
        [cellObjects addObject:newsListCellObject];
    }
    return cellObjects;
}

- (void)updateTableViewModel {
    NSArray *cellObjects = [self generateCellObjects];
    
    self.tableViewModel = [[NIMutableTableViewModel alloc] initWithSectionedArray:cellObjects
                                                                         delegate:(id)[NICellFactory class]];
}

@end
