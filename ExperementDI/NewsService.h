//
//  NewsService.h
//  ExperementDI
//
//  Created by Alex Zverev on 10.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^NewsCompletionBlock)(id data, NSError *error);

@protocol NewsService <NSObject>

- (void)updateNewsWithCompletionBlock:(NewsCompletionBlock)completionBlock;

@end
