//
//  NewsServiceImplementation.m
//  ExperementDI
//
//  Created by Alex Zverev on 10.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "NewsServiceImplementation.h"
#import "RSSNewsParserImplementation.h"
#import "RSSNewsLoaderImplementation.h"
#import "EXTScope.h"

@interface NewsServiceImplementation()

@property (nonatomic, copy) NewsCompletionBlock completed;
@property (nonatomic, strong) NSMutableArray *newsItemPlanedObjects;

@end


@implementation NewsServiceImplementation

- (void)updateNewsWithCompletionBlock:(NewsCompletionBlock)completionBlock {
    self.completed = completionBlock;
    
    __block int loadedArgregatorsCount = 0;
    __block int errorsCount = 0;

    self.newsItemPlanedObjects = [NSMutableArray array];
    
    //1. load news from argregators
    for (int i = 0; i < self.rssAgregatorUrlStrings.count; i++) {
        NSAssert([self.rssAgregatorUrlStrings[i] isKindOfClass:[NSString class]], @"rssAgregators array consist not String Object");
        NSString *rssAgregatorURL = self.rssAgregatorUrlStrings[i];
        @weakify(self)
        [self.loader loadFromRssAgregatorUrlPath:rssAgregatorURL withCompletionBlock:^(id data, NSError *error) {
            @strongify(self)
            loadedArgregatorsCount++;
            if (!error) {
                //2. parse loaded from each agregator news
                NSArray* planedObjectsFromAgregator = [self.parser getNewsModelsFromData:data];
                //3. add planed objects of news to general news list
                [self.newsItemPlanedObjects addObjectsFromArray:planedObjectsFromAgregator];
            } else {
                errorsCount++;
            }
            // если со всех агрегаторов пришли ошибки
            if (errorsCount == self.rssAgregatorUrlStrings.count) {
                completionBlock(nil, error);
                return;
            }
            //4. если пришел ответ с каждого из агрегаторов то сортируем и отсылаем на уровень выше.
            if (loadedArgregatorsCount == self.rssAgregatorUrlStrings.count) {
                
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"publicationDate" ascending:FALSE];
                [self.newsItemPlanedObjects sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
                
                completionBlock(self.newsItemPlanedObjects, error);
            }
        }];
    }
}

@end
