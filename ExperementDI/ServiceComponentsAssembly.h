//
//  ServiceComponentsAssembly.h
//  ExperementDI
//
//  Created by Alex Zverev on 10.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceComponents.h"

@interface ServiceComponentsAssembly : TyphoonAssembly <ServiceComponents>


@end
