//
//  NewsListInteractor.m
//  ExperementDI
//
//  Created by Alex Zverev on 10.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "NewsListInteractor.h"
#import "NewsItemPlaneObject.h"
#import "NewsListInteractorOutput.h"
#import "NewsService.h"
#import "EXTScope.h"


@implementation NewsListInteractor

#pragma mark - NewsListInteractorInput

- (void)updateNewsList {
    @weakify(self)
    //TODO:ask service for newsList
    [self.newsService updateNewsWithCompletionBlock:^(id data, NSError *error) {
            @strongify(self);
        if (error) {
            [self.output didRecievErrorWhenUpdateNewsList];
        } else {
            [self.output didUpdateNewsList:data];
        }
    }];
}

- (NSArray *)obtainNewsList {
//    self.newsService
//    id managedObjectEvents = [self.eventService obtainEventWithPredicate:nil];
//    
//    NSArray *events = [self getPlainNewsFromDictionaris:managedObjectEvents];
//    
    return nil;
}

#pragma mark - Private methods

//- (NSArray *)getPlainNewsFromDictionaris:(NSArray *)manajedObjectEvents {
//    NSMutableArray *newsListPlainObjects = [NSMutableArray array];
//    for (NSString *managedObjectEvent in manajedObjectEvents) {
//        NewsItemPlaneObject *newsItemPlainObject = [NewsItemPlaneObject new];
//        newsItemPlainObject.title = managedObjectEvent;
//        newsItemPlainObject.coverUrl = [NSURL URLWithString:@""];
//        newsItemPlainObject.newsSource = @"lenta.ru";
//        newsItemPlainObject.newsDesciption = @"";
//        //TODO: fill planeObject with managedObject
////        [self.eventPrototypeMapper fillObject:eventPlainObject withObject:managedObjectEvent];
//        
//        [newsListPlainObjects addObject:newsItemPlainObject];
//    }
//    
//    return newsListPlainObjects;
//}

@end
