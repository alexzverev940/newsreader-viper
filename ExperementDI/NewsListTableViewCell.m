//
//  NewsListTableViewCell.m
//  ExperementDI
//
//  Created by Alex Zverev on 09.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "NewsListTableViewCell.h"
#import "NewsListTableViewCellObject.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation NewsListTableViewCell

static NSString *const kPlaceholderImageName = @"placeholder";
static CGFloat const NewsListTableViewCellHeight = 120.f;
static CGFloat const SelectedNewsListTableViewCellHeight = 290.f;



#pragma mark - NICell methods

- (BOOL)shouldUpdateCellWithObject:(NewsListTableViewCellObject *)object {
    
    self.newsTitle.text = object.newsTitle;
    self.newsDescription.text = object.newsDescription;
    self.newsSource.text = object.newsSource;
    self.publicationDate.text = object.publicationDate;
    [self.newsCoverImage sd_setImageWithURL:object.newsCoverUrl
                           placeholderImage:[UIImage imageNamed:kPlaceholderImageName]];
    [self.newsDescription setEnabled:object.isSelected];
    [self.newsDescription setHidden:!object.isSelected];

    return YES;
}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView {
    if (((NewsListTableViewCellObject *)object).isSelected) {
        return SelectedNewsListTableViewCellHeight;
    }
    return NewsListTableViewCellHeight;
}

+ (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

@end
