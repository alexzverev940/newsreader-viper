//
//  NewsListDataDisplayManager.h
//  ExperementDI
//
//  Created by Alex Zverev on 09.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class NewsItemPlaneObject;

@protocol NewsListDataDisplayManagerDelegate

- (void)didUpdateTableViewModel;
- (void)didTapCellWithNewsItem:(NewsItemPlaneObject *)newsItem;

@end


@interface NewsListDataDisplayManager : NSObject

@property (weak, nonatomic) id <NewsListDataDisplayManagerDelegate> delegate;

- (id<UITableViewDataSource>)dataSourceForTableView:(UITableView *)tableView;
- (id<UITableViewDelegate>)delegateForTableView:(UITableView *)tableView withBaseDelegate:(id <UITableViewDelegate>)baseTableViewDelegate;
- (void)updateTableViewModelWithNewsList:(NSArray *)newsList;

@end
