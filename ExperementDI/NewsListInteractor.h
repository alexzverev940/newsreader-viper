//
//  NewsListInteractor.h
//  ExperementDI
//
//  Created by Alex Zverev on 10.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewsListInteractorInput.h"

@protocol NewsService;
@protocol NewsListInteractorOutput;

@interface NewsListInteractor : NSObject<NewsListInteractorInput>

@property (nonatomic, weak) id<NewsListInteractorOutput> output;
//TODO: place FOr Services
@property (strong, nonatomic) id <NewsService> newsService;
//@property (strong, nonatomic) id <PrototypeMapper> eventPrototypeMapper;
@end
