//
//  NewsItemPlaneObject.h
//  ExperementDI
//
//  Created by Alex Zverev on 09.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsItemPlaneObject : NSObject

@property (nonatomic, strong, readwrite) NSString *title;
@property (nonatomic, strong, readwrite) NSURL *coverUrl;
@property (nonatomic, strong, readwrite) NSString *name;
@property (nonatomic, strong, readwrite) NSString *newsDesciption;
@property (nonatomic, strong, readwrite) NSString *newsSource;
@property (nonatomic, strong, readwrite) NSString *publicationDate;

@end
