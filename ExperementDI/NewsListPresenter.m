//
//  NewsListPresenter.m
//  ExperementDI
//
//  Created by Alex Zverev on 09.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "NewsListPresenter.h"
#import "NewsListInteractorInput.h"
#import "NewsListViewInput.h"
#import "NewsItemPlaneObject.h"

@implementation NewsListPresenter

#pragma mark - NewsListViewOutput

- (void)setupView {
    [self.view setupViewWithNewsList:nil];
    [self.view showSpinner];
    [self.interactor updateNewsList];
}

- (void)didTriggerTapCellWithNewsItem:(NewsItemPlaneObject *)newsItem {
}

#pragma mark - NewsListInteractorOutput

- (void)didUpdateNewsList:(NSArray *)newsList {
    [self.view updateViewWithNewsList:newsList];
    [self.view hideSpinner];
}

- (void)didRecievErrorWhenUpdateNewsList {
    [self.view hideSpinner];
    [self.view showNetworkProblemView];
}

@end
