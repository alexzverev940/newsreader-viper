//
//  NewsListViewOutput.h
//  ExperementDI
//
//  Created by Alex Zverev on 09.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NewsItemPlaneObject;

@protocol NewsListViewOutput <NSObject>

- (void)setupView;
- (void)didTriggerTapCellWithNewsItem:(NewsItemPlaneObject *)newsItem;

@end
