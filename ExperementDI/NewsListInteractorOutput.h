//
//  AnnouncementListInteractorOutput.h
//  ExperementDI
//
//  Created by Alex Zverev on 09.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NewsListInteractorOutput <NSObject>

/**
 Method is used to inform presenter that list of news has been updated
 
 @param newsList Array of NewsItemPlameObjects
*/
- (void)didUpdateNewsList:(NSArray *)newsList;

/**
 Method is used to inform presenter that error occured while list updating
 */
- (void)didRecievErrorWhenUpdateNewsList;

@end
