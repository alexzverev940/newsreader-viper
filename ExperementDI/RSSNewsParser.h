//
//  RSSNewsParser.h
//  ExperementDI
//
//  Created by Alex Zverev on 14.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RSSNewsParser <NSObject>

- (NSArray *)getNewsModelsFromData:(id) data;

@end
