//
//  NewsListModuleAssembly.m
//  ExperementDI
//
//  Created by Alex Zverev on 10.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "NewsListModuleAssembly.h"
#import "NewsListTableViewController.h"
#import "NewsListInteractor.h"
#import "NewsListPresenter.h"
#import "NewsListRouter.h"
#import "NewsListDataDisplayManager.h"
#import "ServiceComponents.h"


@implementation NewsListModuleAssembly
- (NewsListTableViewController *)viewNewsList {
    return [TyphoonDefinition withClass:[NewsListTableViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterNewsList]];
                              [definition injectProperty:@selector(dataDisplayManager)
                                                    with:[self dataDisplayManagerNewsList]];
                          }];
}

- (NewsListInteractor *)interactorNewsList {
    return [TyphoonDefinition withClass:[NewsListInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterNewsList]];
                              [definition injectProperty:@selector(newsService)
                                                    with:[self.serviceComponents newsService]];
                          }];
}

- (NewsListPresenter *)presenterNewsList {
    return [TyphoonDefinition withClass:[NewsListPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewNewsList]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorNewsList]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerNewsList]];
                          }];
}

- (NewsListRouter *)routerNewsList {
    return [TyphoonDefinition withClass:[NewsListRouter class]];
}

- (NewsListDataDisplayManager *)dataDisplayManagerNewsList {
    return [TyphoonDefinition withClass:[NewsListDataDisplayManager class]];
}


@end
