//
//  NewsListModuleAssembly.h
//  ExperementDI
//
//  Created by Alex Zverev on 10.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Typhoon/Typhoon.h>

@protocol ServiceComponents;

@interface NewsListModuleAssembly : TyphoonAssembly
@property (strong, nonatomic, readonly) TyphoonAssembly <ServiceComponents> *serviceComponents;

@end
