//
//  NewsListPresenter.h
//  ExperementDI
//
//  Created by Alex Zverev on 09.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewsListViewOutput.h"
#import "NewsListInteractorOutput.h"

@protocol NewsListViewInput;
@protocol NewsListInteractorInput;
@protocol NewsListRouterInput;


@interface NewsListPresenter : NSObject<NewsListInteractorOutput, NewsListViewOutput>

@property (nonatomic, weak) id<NewsListViewInput> view;
@property (nonatomic, strong) id<NewsListInteractorInput>  interactor;
@property (nonatomic, strong) id<NewsListRouterInput> router;

@end
