//
//  NewsListTableViewCellObject.m
//  ExperementDI
//
//  Created by Alex Zverev on 09.06.16.
//  Copyright © 2016 argas. All rights reserved.
//
#import "NewsListTableViewCell.h"
#import "NewsListTableViewCellObject.h"

@implementation NewsListTableViewCellObject

#pragma mark - Initialization

- (instancetype)initWithNewsItem:(NewsItemPlaneObject *)newsItem {
    self = [super init];
    if (self) {
        _newsTitle = newsItem.title;
        _newsDescription = newsItem.newsDesciption;
        _newsSource = newsItem.newsSource;
        _newsCoverUrl = newsItem.coverUrl;
        _publicationDate = newsItem.publicationDate;
        _newsItem = newsItem;
    }
    return self;
}

+ (instancetype)objectWithNewsItem:(NewsItemPlaneObject *)newsItem {
    return [[self alloc] initWithNewsItem:newsItem];
}

#pragma mark - NICellObject methods

- (Class)cellClass {
    return [NewsListTableViewCell class];
}

- (UINib *)cellNib {
    return [UINib nibWithNibName:NSStringFromClass([NewsListTableViewCell class]) bundle:[NSBundle mainBundle]];
}

@end
