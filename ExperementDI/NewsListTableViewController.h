//
//  NewsListTableTableViewController.h
//  ExperementDI
//
//  Created by Alex Zverev on 09.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsListViewInput.h"

@protocol NewsListViewOutput;
@class NewsListDataDisplayManager;

@interface NewsListTableViewController : UITableViewController<NewsListViewInput>

@property (nonatomic, strong) id<NewsListViewOutput> output;
@property (strong, nonatomic) NewsListDataDisplayManager *dataDisplayManager;//TODO: inject it

@end
