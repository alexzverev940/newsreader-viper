//
//  NewsListRouter.h
//  ExperementDI
//
//  Created by Alex Zverev on 10.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewsListRouterInput.h"

@interface NewsListRouter : NSObject<NewsListRouterInput>

@end
