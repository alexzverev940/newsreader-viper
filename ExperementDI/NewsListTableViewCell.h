//
//  NewsListTableViewCell.h
//  ExperementDI
//
//  Created by Alex Zverev on 09.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NICellFactory.h"

@interface NewsListTableViewCell : UITableViewCell <NICell>
@property (weak, nonatomic) IBOutlet UILabel *publicationDate;
@property (weak, nonatomic) IBOutlet UILabel *newsTitle;
@property (weak, nonatomic) IBOutlet UILabel *newsDescription;
@property (weak, nonatomic) IBOutlet UIImageView *newsCoverImage;
@property (weak, nonatomic) IBOutlet UILabel *newsSource;
@end
