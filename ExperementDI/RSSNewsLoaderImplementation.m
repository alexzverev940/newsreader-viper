//
//  RSSNewsLoaderImplementation.m
//  ExperementDI
//
//  Created by Alex Zverev on 14.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "RSSNewsLoaderImplementation.h"
#import "NewsItemPlaneObject.h"

@implementation RSSNewsLoaderImplementation

- (void)loadFromRssAgregatorUrlPath:(NSString *)rssAgregatorUrl withCompletionBlock:(RssCompletionBlock)onComplete {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:rssAgregatorUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        onComplete(data, error);
    }];
    
    [postDataTask resume];
}

@end
