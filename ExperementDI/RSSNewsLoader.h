//
//  RssNewsLoader.h
//  ExperementDI
//
//  Created by Alex Zverev on 14.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^RssCompletionBlock)(id data, NSError *error);

@protocol RSSNewsLoader <NSObject>

- (void)loadFromRssAgregatorUrlPath:(NSString *)rssAgregatorUrl withCompletionBlock:(RssCompletionBlock)onComplete;

@end
