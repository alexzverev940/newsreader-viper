//
//  NewsServiceImplementation.h
//  ExperementDI
//
//  Created by Alex Zverev on 10.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewsService.h"

@protocol RSSNewsParser;
@protocol RSSNewsLoader;


@interface NewsServiceImplementation : NSObject<NewsService>

@property (nonatomic, strong) NSArray *rssAgregatorUrlStrings;
@property (nonatomic, strong) id <RSSNewsParser> parser;
@property (nonatomic, strong) id <RSSNewsLoader> loader;

@end
