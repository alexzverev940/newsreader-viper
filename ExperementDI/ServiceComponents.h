//
//  ServiceComponents.h
//  ExperementDI
//
//  Created by Alex Zverev on 10.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Typhoon/Typhoon.h>

@protocol NewsService;

@protocol ServiceComponents <NSObject>

- (id<NewsService>)newsService;
@end
