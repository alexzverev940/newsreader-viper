//
//  RSSNewsParserImplementation.m
//  ExperementDI
//
//  Created by Alex Zverev on 14.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "RSSNewsParserImplementation.h"
#import "NewsItemPlaneObject.h"
#import "XMLDictionary.h"
#import "NSDate+RSS.h"

@implementation RSSNewsParserImplementation

-(NSArray *)getNewsModelsFromData:(id)data {
    NSArray *arrayOfData;
    NSMutableArray *arrayOfPlanedObjects;
    if (data) {
        NSDictionary *result = [NSDictionary dictionaryWithXMLData:data];
        arrayOfData = result[@"channel"][@"item"];
        arrayOfPlanedObjects = [NSMutableArray new];
        [arrayOfData enumerateObjectsUsingBlock:^(NSDictionary *item, NSUInteger idx, BOOL * _Nonnull stop) {
            NewsItemPlaneObject *obj = [NewsItemPlaneObject new];
            obj.title = item[@"title"];
            obj.newsDesciption = item[@"description"];
            obj.newsSource =  result[@"channel"][@"title"];
            NSString *timeStampString = item[@"pubDate"];
            if (timeStampString) {
                NSDate *d = [NSDate dateWithPubDate:timeStampString];
                NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
                [_formatter setDateFormat:@"dd.mm.yy : HH:mm"];
                obj.publicationDate = [_formatter stringFromDate:d];
            } else {obj.publicationDate = @"-";}
            
            id coverUrlDictionary = item[@"enclosure"];
            if (coverUrlDictionary) {
                NSString *coverUrl = coverUrlDictionary[@"_url"];
                obj.coverUrl = [NSURL URLWithString:coverUrl];
            }
            if (timeStampString) {
                NSDate *d = [NSDate dateWithPubDate:timeStampString];
                NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
                [_formatter setDateFormat:@"dd.MM.yyyy | HH:mm"];
                obj.publicationDate = [_formatter stringFromDate:d];
            } else {
                obj.publicationDate = @"-";
            }
 
            [arrayOfPlanedObjects addObject:obj];
        }];
    } else {
        
    }
    return arrayOfPlanedObjects;
}

@end
