//
//  NewsListViewInput.h
//  ExperementDI
//
//  Created by Alex Zverev on 09.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NewsListViewInput <NSObject>

/**
 Method is used to set up current view with news
 
 @param events Array of PlainNewsItem objects
 */
- (void)setupViewWithNewsList:(NSArray *)newsList;

/**
 Method is used to update current view with news
 
 @param events Array of PlainNewsItem objects
 */
- (void)updateViewWithNewsList:(NSArray *)newsList;

- (void)showNetworkProblemView;

- (void)showSpinner;

- (void)hideSpinner;

@end
