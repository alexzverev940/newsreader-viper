//
//  ServiceComponentsAssembly.m
//  ExperementDI
//
//  Created by Alex Zverev on 10.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "ServiceComponentsAssembly.h"
#import "NewsServiceImplementation.h"
#import "RSSNewsParserImplementation.h"
#import "RSSNewsLoaderImplementation.h"

static NSString *const kLentaSource = @"https://lenta.ru/rss";
static NSString *const kGazetaSource = @"http://www.gazeta.ru/export/rss/lenta.xml";


@implementation ServiceComponentsAssembly

- (id <NewsService>)newsService {
    return [TyphoonDefinition withClass:[NewsServiceImplementation class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(rssAgregatorUrlStrings)
                                                    with:@[kLentaSource,
                                                           kGazetaSource]];
                              [definition injectProperty:@selector(parser)
                                                    with:[self rssNewsParser]];
                              [definition injectProperty:@selector(loader)
                                                    with:[self rssNewsLoader]];
                          }
            ];
}

- (id <RSSNewsParser>)rssNewsParser {
    return [TyphoonDefinition withClass:[RSSNewsParserImplementation class]];
}

- (id <RSSNewsLoader>)rssNewsLoader {
    return [TyphoonDefinition withClass:[RSSNewsLoaderImplementation class]];
}
@end
