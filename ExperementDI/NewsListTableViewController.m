//
//  NewsListTableTableViewController.m
//  ExperementDI
//
//  Created by Alex Zverev on 09.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "NewsListTableViewController.h"
#import "NewsListViewOutput.h"
#import "NewsListDataDisplayManager.h"
#import "NewsItemPlaneObject.h"

static NSString *const kNetworkProblemMessage = @"Проблемы с сетью";

@interface NewsListTableViewController ()<NewsListDataDisplayManagerDelegate>
@property (nonatomic, strong)UIActivityIndicatorView *spinner;
@property (nonatomic, strong)UILabel *networkProblemMessage;

@end

@implementation NewsListTableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.output setupView];
    [self configureTableView];
}

#pragma mark - lazy initialization
-(UIActivityIndicatorView *)spinner {
    if(!_spinner) {
        _spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:(UIActivityIndicatorViewStyleGray)];
        [_spinner setCenter:self.view.center];
        _spinner.hidesWhenStopped = YES;
        [self.view addSubview:_spinner];
    }
    return _spinner;
}

- (UILabel *)networkProblemMessage {
    //TODO: вынести в отдельный вью класс
    if (!_networkProblemMessage) {
        _networkProblemMessage = [[UILabel alloc]init];
        _networkProblemMessage.text = kNetworkProblemMessage;
        [_networkProblemMessage sizeToFit];
        _networkProblemMessage.center = self.view.center;
        _networkProblemMessage.hidden = YES;
        [self.view addSubview:_networkProblemMessage];
    }
    return _networkProblemMessage;
}

#pragma mark - NewsListDataDisplayManagerDelegate

- (void)didUpdateTableViewModel {
    self.tableView.dataSource = [self.dataDisplayManager dataSourceForTableView:self.tableView];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });    
}

- (void)didTapCellWithNewsItem:(NewsItemPlaneObject *)newsItem {
    //send Tap to presetner
    [self.output didTriggerTapCellWithNewsItem:newsItem];
//        NSLog(@"== tapped: %@", newsItem.newsDesciption);
}

#pragma mark - NewsListViewInput

- (void)setupViewWithNewsList:(NSArray *)newsList {
    [self updateViewWithNewsList:newsList];
    self.dataDisplayManager.delegate = self;
    
    self.tableView.dataSource = [self.dataDisplayManager dataSourceForTableView:self.tableView];
    self.tableView.delegate = [self.dataDisplayManager delegateForTableView:self.tableView withBaseDelegate:nil];
}

- (void)updateViewWithNewsList:(NSArray *)newsList {
    [self.dataDisplayManager updateTableViewModelWithNewsList:newsList];
}

- (void)showNetworkProblemView {
    [self.networkProblemMessage setHidden:NO];
}

- (void)showSpinner {
    [self.spinner startAnimating];
}

- (void)hideSpinner {
    [self.spinner stopAnimating];
}

#pragma mark - Private methods


- (void)configureTableView {
    [self.tableView setContentInset:UIEdgeInsetsMake(20, 0, 0, 0)];
    self.tableView.estimatedRowHeight = 120;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView setNeedsLayout];
    [self.tableView layoutIfNeeded];
}
@end
