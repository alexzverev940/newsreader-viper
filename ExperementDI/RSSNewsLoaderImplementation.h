//
//  RSSNewsLoaderImplementation.h
//  ExperementDI
//
//  Created by Alex Zverev on 14.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSSNewsLoader.h"

@interface RSSNewsLoaderImplementation : NSObject<RSSNewsLoader>

@end
