//
//  NewsServiseTests.m
//  ExperementDI
//
//  Created by Alex Zverev on 15.06.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "NewsServiceImplementation.h"
#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "RSSNewsLoader.h"

static NSString *const kLentaSource = @"https://lenta.ru/rss";


@interface NewsServiseTests : XCTestCase
@property (strong, nonatomic) NewsServiceImplementation *newsService;

@end

@implementation NewsServiseTests

- (void)setUp {
    [super setUp];
    
    self.newsService = [NewsServiceImplementation new];
}

- (void)tearDown {
    self.newsService = nil;
    
    [super tearDown];
}

- (void)testSuccessNewsUpdate {
    // given
    id <RSSNewsLoader> mockRssLoader = OCMProtocolMock(@protocol(RSSNewsLoader));
    self.newsService.rssAgregatorUrlStrings = @[kLentaSource];
    self.newsService.loader = mockRssLoader;
    
    // when
    [self.newsService updateNewsWithCompletionBlock:nil];
    
    // then
    OCMVerify([mockRssLoader loadFromRssAgregatorUrlPath:self.newsService.rssAgregatorUrlStrings[0] withCompletionBlock:[OCMArg any]]);
}



@end
