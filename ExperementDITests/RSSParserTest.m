//
//  ParserTest.m
//  ExperementDI
//
//  Created by Alex Zverev on 15.06.16.
//  Copyright © 2016 argas. All rights reserved.
//
#import "RSSNewsParserImplementation.h"
#import "NewsItemPlaneObject.h"
#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

@interface ParserTest : XCTestCase

@property (nonatomic, strong) RSSNewsParserImplementation *parser;

@end

@implementation ParserTest

- (void)setUp {
    [super setUp];
    
    self.parser = [RSSNewsParserImplementation new];
}

- (void)tearDown {
    
    self.parser = nil;
    
    [super tearDown];
}

- (void)testExample {
    //given
    NSString *dataSting = @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\
    <rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">\
    <channel>\
    <language>ru</language>\
    <title>GAZETA</title>\
    <description>Новости, статьи, фотографии, видео. Семь дней в неделю, 24 часа в сутки</description>\
    <link>http://lenta.ru</link>\
    <image>\
    <url>http://assets.lenta.ru/small_logo.png</url>\
    <title>Lenta.ru</title>\
    <link>http://lenta.ru</link>\
    <width>134</width>\
    <height>22</height>\
    </image>\
    <atom:link rel=\"self\" type=\"application/rss+xml\" href=\"http://lenta.ru/rss\"/>\
    <item>\
    <guid>http://lenta.ru/news/2016/06/15/su30/</guid>\
    <title>Title1</title>\
    <link>http://lenta.ru/news/2016/06/15/su30/</link>\
    <description>\
    <![CDATA[Описание первое]]>\
    </description>\
    <pubDate>Wed, 15 Jun 2016 08:55:00 +0300</pubDate>\
    <enclosure url=\"https:coverUrl\" length=\"17318\" type=\"image/jpeg\"/>\
    <category>Мир</category>\
    </item>\
    <item>\
    <guid>http://lenta.ru/news/2016/06/14/bonehormone/</guid>\
    <title>Ученые обратили вспять старение мышц</title>\
    <link>http://lenta.ru/news/2016/06/14/bonehormone/</link>\
    <description>\
    <![CDATA[Физиологи из Колумбийского университета выяснили, что во время физических тренировок человеческие кости производят гормон остеокальцин, который увеличивает производительность мышц. Выработка остеокальцина в организме снижается с возрастом, начиная с 30 лет у женщин и с 50 лет у мужчин. ]]>\
    </description>\
    <pubDate>Wed, 15 Jun 2016 08:36:00 +0300</pubDate>\
    <enclosure url=\"https://icdn.lenta.ru/images/2016/06/14/16/20160614163917128/pic_ef28201818c5edd2723c6e031a877745.jpg\" length=\"40767\" type=\"image/jpeg\"/>\
    </item>";
    NSData *rssTestData1 = [dataSting dataUsingEncoding:NSUTF8StringEncoding];
    
    //when
    NSArray *arrayOfPlaneObjects = [self.parser getNewsModelsFromData:rssTestData1];
    NewsItemPlaneObject* newsPLaneObject =  arrayOfPlaneObjects[0];

    //then
    XCTAssertTrue([arrayOfPlaneObjects[0] isKindOfClass:[NewsItemPlaneObject class]]);
    XCTAssertTrue(arrayOfPlaneObjects.count == 2);
    XCTAssertTrue([newsPLaneObject.title isEqualToString:@"Title1"]);
    XCTAssertTrue([newsPLaneObject.newsDesciption isEqualToString:@"Описание первое"]);
    XCTAssertTrue([newsPLaneObject.newsSource isEqualToString:@"GAZETA"]);
    XCTAssertTrue([newsPLaneObject.coverUrl isEqual: [NSURL URLWithString:@"https:coverUrl"]]);
}

@end
